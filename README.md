# S61.20

## How To Grow (almost) Anything 2021
[http://fab.cba.mit.edu/classes/S61.20/](http://fab.cba.mit.edu/classes/S61.20/)

## About
Building upon the tradition of ‘How to Make (Almost) Anything,’ we are offering ‘How to Grow (Almost) Anything,’ a course to teach experienced bio-enthusiasts and those new to the life sciences alike skills at the cutting edge of bioengineering and synthetic biology. 