var app = new Vue({
  el: '#app',
  data: {
    trigger: null,
    list: [
        {text: "Eyal Perry", color: "#000000"},
        {text: "Manvitha Ponnapati", color: "#000000"},
        {text: "Lynced Torres", color: "#000000"},
        {text: "Thomas Smith", color: "#000000"},
        {text: "Joe Faraguna", color: "#000000"},
        {text: "Anjali Chadha", color: "#000000"},
        {text: "Belen Vicente Blazquez", color: "#000000"},
        {text: "Eyal Perry", color: "#000000"},
        {text: "Manvitha Ponnapati", color: "#000000"},
        {text: "Lynced Torres", color: "#000000"},
        {text: "Thomas Smith", color: "#000000"},
        {text: "Joe Faraguna", color: "#000000"},
        {text: "Anjali Chadha", color: "#000000"},
        {text: "Belen Vicente Blazquez", color: "#000000"},
    ]
  },
  methods: {
    start() {
        this.trigger = new Date();
    }
  },
  components: { "slot-machine": VueSlotMachine.SlotMachine }
})
